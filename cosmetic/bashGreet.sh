# ~/scripts/cosmetic/bashGreet.sh

#!/bin/bash



### Variables

user=$(echo $(tput bold setaf 11)'['$(tput bold setaf 9)$HOSTNAME$(tput bold setaf 11)' > '$(tput bold setaf 10)$USER$(tput bold setaf 11)']')
length=$(echo '['$(cat /proc/sys/kernel/hostname)' > '$(whoami)']')
os="Arch Linux $HOSTTYPE"
host="Satellite L50-C"
kern=
up=
pack="$(paru -Q | wc --lines) - Paru"
sh="Bash"
res="$(xrandr | grep '*' | awk -F ' ' '{print $1}')"
fps="$(xrandr | grep '*' | awk -F ' ' '{print $2}' | cut -d '.' -f1) Hz"
wm="XMonad"
term="Alacritty"
font="Nerd Fonts Source Code Pro"
core=$(lscpu | grep 'per socket' | awk -F ' ' '{printf $4}')
thread=$(lscpu | grep 'per core' | awk -F ' ' '{printf $4}')
freqMin=$(lscpu | grep 'min MHz' | awk -F ' ' '{printf $4}' | cut -d '.' -f1)
freqMax=$(lscpu | grep 'max MHz' | awk -F ' ' '{printf $4}' | cut -d '.' -f1)
cpu="Intel i5-6200U ("$core"x"$thread") "$freqMin"-"$freqMax" MHz"
igpu="HD Grpahics 520"
dgpu="NVIDIA GeForce 930M"
ram=
ssd=



### Output

echo $(tput sgr0 setaf 0)
echo $(tput bold setaf 4)'                     -'$(tput bold setaf 12)'`                     '$user
echo $(tput bold setaf 4)'                    .o'$(tput bold setaf 12)'+`                    '$(perl -E "say '-' x ${#length}")
echo $(tput bold setaf 4)'                   `oo'$(tput bold setaf 12)'o/                    ''CPU: '$(tput sgr0 setaf 15)$cpu
echo $(tput bold setaf 4)'                  `+oo'$(tput bold setaf 12)'oo:                   ''iGPU: '
echo $(tput bold setaf 4)'                 `+ooo'$(tput bold setaf 12)'ooo:                  ''dGPU: '
echo $(tput bold setaf 4)'                 -+ooo'$(tput bold setaf 12)'ooo+:                 ''Memory: '
echo $(tput bold setaf 4)'               `/:-:++'$(tput bold setaf 12)'oooo+:                ''Disk: '
echo $(tput bold setaf 4)'              `/++++/+'$(tput bold setaf 12)'++++++:               ''Resolution:'
echo $(tput bold setaf 4)'             `/+++++++'$(tput bold setaf 12)'+++++++:              ''Refresh Rate: '
echo $(tput bold setaf 4)'            `/+++ooooo'$(tput bold setaf 12)'oooooooo/`            ''OS: '
echo $(tput bold setaf 4)'           ./ooosssso+'$(tput bold setaf 12)'+osssssso+`           ''Host: '
echo $(tput bold setaf 4)'          .oossssso-``'$(tput bold setaf 12)'``/ossssss+`          ''Architecture: '
echo $(tput bold setaf 4)'         -osssssso.   '$(tput bold setaf 12)'   :ssssssso.         ''Kernel: '
echo $(tput bold setaf 4)'        :osssssss/    '$(tput bold setaf 12)'    osssso+++.        ''Shell: '
echo $(tput bold setaf 4)'       /ossssssss/    '$(tput bold setaf 12)'    +ssssooo/-        ''Window Manager: '
echo $(tput bold setaf 4)'     `/ossssso+/:-    '$(tput bold setaf 12)'    -:/+osssso+-      ''Terminal: '
echo $(tput bold setaf 4)'    `+sso+:-`         '$(tput bold setaf 12)'        `.-/+oso:     ''Font: '
echo $(tput bold setaf 4)'   `++:.              '$(tput bold setaf 12)'             `-/+/    ''Packages: '
echo $(tput bold setaf 4)'  .`                  '$(tput bold setaf 12)'               `/     ''Uptime: '
echo $(tput sgr0 setaf 0)


