# ~/scripts/xmobar/cputemp.sh

#!/bin/bash



### Get CPU-Package Temperature (Options):

temp=$(sensors | grep id | cut -d + -f2 | cut -d . -f1)
#temp=$(cat /sys/class/hwmon/hwmon3/temp1_input | cut -c1-2)



### Set Low and High Cutoff:

low=40
high=60




### Set Color based on Temperature and Threshholds: 

#blue="<fc=#729fcf>"
green="<fc=#8ae234>"
yellow="<fc=#fce94f>"
red="<fc=#ef2929>"

color=""

if [[ temp -le low ]]; then
	color=$green
elif [[ temp -ge high ]]; then
	color=$red
else 
	color=$yellow
fi



### Output

echo $color$temp"</fc>"
