# ~/scripts/dmenu/usermenu.sh

#!/bin/bash



function powermenu {

	options="Back\nShutdown\nRestart\nSleep"

	selected=$(echo -e $options | dmenu -p "Power:")

	if   [[ $selected = "Shutdown" ]]; then # Turn Off Machine
		shutdown now
	elif [[ $selected = "Restart" ]]; then # Restart Machine
		reboot
	elif [[ $selected = "Sleep" ]]; then # Hibernate Machine
		return
	elif [[ $selected = "Back" ]]; then # User Options
		usermenu
	fi

}



function performancemenu {

	options="Back\nProcessorPowerlimit\nHybridGraphics\nDiscreteGraphics"

	selected=$(echo -e $options | dmenu -p "Performance:")

	if   [[ $selected = "ProcessorPowerlimit" ]]; then # Toggle CPU-Powerlimit
		return
	elif [[ $selected = "HybridGraphics" ]]; then # Toggle MUX-Switch
		return
	elif [[ $selected = "DiscreteGraphics" ]]; then # Turn Off Discrete Graphics When in Hybrid Mode
		return
	elif [[ $selected = "Back" ]]; then # User Options
		usermenu
	fi

}



function usermenu {

	options="Power\nPerformance"

	selected=$(echo -e $options | dmenu -p "Options:")

	if   [[ $selected = "Power" ]]; then # Power Options
		powermenu
	elif [[ $selected = "Performance" ]]; then # Performace Options
		performancemenu
	fi

}



usermenu
